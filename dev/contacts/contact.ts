export interface Contact{
    firstname: string,
    lastname: string,
    email: string
}


export const CONTACTS: Contact[] = [{
    firstname: 'Hariharan',
    lastname: 'Palani',
    email: 'hariharanpalani@live.com'
},{
    firstname: 'Kirubaharan',
    lastname: 'Palani',
    email: 'pkiruba85@gmail.com'
},{
    firstname: 'Hemalatha',
    lastname: 'Palani',
    email: 'pkiruba85@gmail.com'
}];
