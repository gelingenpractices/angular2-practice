import { ControlGroup } from 'angular2/common';
import {Component} from 'angular2/core';
import {ContactService} from './contact-service';
import {Contact} from './contact';
import {Router} from 'angular2/router';
import {RouteParams} from 'angular2/router';
import {OnInit} from 'angular2/core';
import {FormBuilder} from 'angular2/common';
import {Validators} from 'angular2/common';

@Component({
    template: `
        <form [ngFormModel]="myForm" (ngSubmit)="onSubmit(myForm.value)">
            <div>
                <label for="firstname">Firstname:</label>
                <input type="text" id="firstname" [ngFormControl]="myForm.controls['firstname']" #firstname="ngForm">
                <span *ngIf="!firstname.valid">Name needed</span>
            </div>
            <div>
                <label for="lastname">Lastname:</label>
                <input type="text" id="lastname" [ngFormControl]="myForm.controls['lastname']">
            </div>
            <div>
                <label for="email">Email:</label>
                <input id="email" type="text" [ngFormControl]="myForm.controls['email']">
            </div>
            <div>
                <button type="submit">Create Contact</button>
            </div>
        </form>
    `,
    providers: [ContactService],
    styles:[`
        label {
            display: inline-block;
            width: 150px;
        }

        input {
            width: 250px;
        }
    `]
})
export class NewContactFormComponent implements OnInit{ 
    myForm: ControlGroup;

    constructor(private _contactService: ContactService, private _router: Router,
    private _routeParams: RouteParams, private _formBuilder: FormBuilder){

    }

    /*OnAddContacts(firstName, lastName, email){
        let contact: Contact = {
            firstname: firstName,
            lastname: lastName,
            email: email
        };
        this._contactService.insertContacts(contact);
        this._router.navigate(['Contacts']);
    }*/

    onSubmit(value){
        this._contactService.insertContacts(value);
        this._router.navigate(['Contacts']);
    }

    ngOnInit(): any {
        this.myForm = this._formBuilder.group({
            'firstname': ['', Validators.required],
            'lastname': [this._routeParams.get('lastname'), Validators.required],
            'email': []
        });
    }
}