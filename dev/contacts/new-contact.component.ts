import {Component} from 'angular2/core';
import {ContactService} from './contact-service';
import {Contact} from './contact';
import {Router} from 'angular2/router';
import {RouteParams} from 'angular2/router';
import {OnInit} from 'angular2/core';

@Component({
    template: `
        <form #myForm="ngForm" (ngSubmit)="onSubmit(myForm)" novalidate>
            <div>
                <label for="firstname">Firstname:</label>
                <input type="text" id="firstname" 
                    ngControl="name" 
                    required
                    [(ngModel)]="newContact.firstname">
            </div>
            <div>
                <label for="lastname">Lastname:</label>
                <input type="text" id="lastname" 
                        ngControl="lastName" 
                        [(ngModel)]="newContact.lastname"
                        required>
            </div>
            <div>
                <label for="email">Email:</label>
                <input id="email" type="text" ngControl="Email" 
                    [(ngModel)]="newContact.email"
                required>
            </div>
            <div>
                <button type="submit">Create Contact</button>
            </div>
        </form>
    `,
    providers: [ContactService],
    styles:[`
        label {
            display: inline-block;
            width: 150px;
        }

        input {
            width: 250px;
        }
    `]
})
export class NewContactComponent implements OnInit{ 
    newContact: Contact;

    constructor(private _contactService: ContactService, private _router: Router,
    private _routeParams: RouteParams){

    }

    /*OnAddContacts(firstName, lastName, email){
        let contact: Contact = {
            firstname: firstName,
            lastname: lastName,
            email: email
        };
        this._contactService.insertContacts(contact);
        this._router.navigate(['Contacts']);
    }*/

    onSubmit(form: any){
        this._contactService.insertContacts(this.newContact);
        this._router.navigate(['Contacts']);
    }

    ngOnInit(): any {
        this.newContact = {lastname: this._routeParams.get('lastname'),
            firstname: '',
            email: ''
        };
    }
}