import {Component} from 'angular2/core';
import {Router} from 'angular2/router';
import {Contact} from './contact';

@Component({
    selector: 'contact',
    template: `
        <div>
            <div>
                <label for="firstname">Firstname:</label>
                <input type="text" [(ngModel)]="contact.firstname" id="firstname">
            </div>
            <div>
                <label for="lastname">Lastname:</label>
                <input type="text" [(ngModel)]="contact.lastname" id="lastname" >
            </div>
            <div>
                <label for="email">Email:</label>
                <input id="email" type="text" [(ngModel)]="contact.email">
            </div>
        </div>
        <button (click)="onCreateNew()">Create new using this contact</button>

    `,
    inputs: ["contact"],
    styles:[`
        label {
            display: inline-block;
            width: 150px;
        }

        input {
            width: 250px;
        }
    `]
})
export class ContactComponent{
    public contact: Contact = null;

    constructor(private _router: Router){

    }

    onCreateNew(){
        this._router.navigate([
            'NewContact', {
                lastname: this.contact.lastname
            }
        ]);
    }
}