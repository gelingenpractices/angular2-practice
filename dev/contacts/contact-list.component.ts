import {Component} from 'angular2/core';
import {ContactComponent} from './contact.component';
import {Contact} from './contact';
import {ContactService} from './contact-service';
import {OnInit} from 'angular2/core';

@Component({
    //selector: 'contact-list',
    template: `
        <ul>
            <li *ngFor="#contact of contacts" 
            (click)="onSelect(contact)" [class.clicked]="selectedContact === contact">
                {{contact.firstname}} {{contact.lastname}}
            </li>
        <ul>
        <contact *ngIf="selectedContact != null" [contact]="selectedContact"></contact>
    `,
    directives: [ContactComponent],
    providers: [ContactService],
    styleUrls: ['../src/css/app.css']
})
export class ContactListComponent implements OnInit{
    public contacts: Contact[];

    public selectedContact = null;

    constructor(private _contractService: ContactService){

    }

    onSelect(contact){
        this.selectedContact = contact;
    }

    getContacts(){
        this._contractService.getContacts().then((contacts: Contact[]) => this.contacts = contacts);
    }

   ngOnInit(){
        this.getContacts();
    }
}