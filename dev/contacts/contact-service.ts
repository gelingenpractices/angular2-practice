import {Injectable} from 'angular2/core';
import {CONTACTS} from './contact';
import {Contact} from './contact';

@Injectable()
export class ContactService{
    getContacts(){
        return Promise.resolve(CONTACTS);
    }

    insertContacts(contact: Contact){
        return Promise.resolve(CONTACTS).then((contacts: Contact[]) => contacts.push(contact));
    }
}